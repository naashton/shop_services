'''
Copyright (C) 2019 Nicholas Ashton - All Rights Reserved
'''

class Inventory:
    '''
    How do we want to handle inventory?
     * Each business could have multiple inventories
     * The makeup of this class:
       - Inventory Table
    '''
    
    def __init__(self):
        self.inventory = {}
        self.db_conn = None
        
    
    def load_inventory(self):
        return