from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from shop_services.auth import login_required
from shop_services.db import get_db

bp = Blueprint('inventory', __name__)

'''
Index method, this is the landing page for our application.

The only thing we should really have to do here is render the template.
'''
@bp.route('/')
def index():
    db = get_db()

    return render_template('index.html', inventory=inventory)

'''
Method to view and edit inventory.
'''
@bp.route('/inventory', methods=('GET', 'POST'))
@login_required
def inventory():
    inventory = get_db().execute('SELECT item_id, item_name, item_type, item_size, item_price,'
    'item_quantity FROM inventory').fetchall()

    return render_template('inventory/inventory.html', inventory=inventory)

'''
Method to create a new item in the inventory
'''
@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST' or request.method == 'GET':
        
        item = request.form['item']
        type = request.form['type']
        size = request.form['size']
        price = request.form['price']
        quantity = request.form['quantity']
        
        error = None
        
#         if not title:
#             error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO inventory (item_name, item_type, item_size, item_price, item_quantity)'
                ' VALUES (?,?,?,?,?)',
                (item, type, size, price, quantity)
            )
            db.commit()
            return redirect(url_for('inventory.inventory'))

    return render_template('inventory/create.html')


'''
Method to get inventory item to update
'''
def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


'''
Method to update an item
'''
@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('inventory.index'))

    return render_template('inventory/update.html', post=post)

'''
Delete an item
'''
@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM inventory WHERE  item_id = ?', (id,))
    db.commit()
    return redirect(url_for('inventory.index'))
